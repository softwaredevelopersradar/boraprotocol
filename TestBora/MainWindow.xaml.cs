﻿using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using BoraClientDll;
using Types;


namespace TestBora
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            BoraClient boraClient = new BoraClient();
            //await boraClient.IQConnectToServer("127.0.0.1", 8888);

            string json = @"{
  'msg_id': 7771,
  'task': {
 'id' : 2239,
'type': 'single',
'action' : 'start',
'processors' : [
'control'
]
},
'control' : {
'att' : {
'value' : '20'
}
}
}";

            BoraProtocol.ControlRequest myDeserializedClass = JsonConvert.DeserializeObject<BoraProtocol.ControlRequest>(json);


            BoraProtocol.ControlRequest account = JsonConvert.DeserializeObject<BoraProtocol.ControlRequest>(json);

            string json2 = JsonConvert.SerializeObject(account, Newtonsoft.Json.Formatting.Indented);


            MemoryStream ms = new MemoryStream();
            using (BsonWriter writer = new BsonWriter(ms))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(writer, account);
            }

            string data = Convert.ToBase64String(ms.ToArray());
            byte[] bytes = ms.ToArray();


            await boraClient.SendRequestMini(bytes);

            ms = new MemoryStream(bytes);
            using (BsonReader reader = new BsonReader(ms))
            {
                JsonSerializer serializer = new JsonSerializer();

                account = serializer.Deserialize<BoraProtocol.ControlRequest>(reader);
            }
        }

        //string ip = "127.0.0.1";
        //string ip = "0.0.0.0";
        string ip = "192.168.0.107";
        //string ip = "192.168.194.26";
        //int port = 8888;
        int port = 6789;

        private BoraClient boraClient = new BoraClient();


        private async void BsonMessageRTP_Click(object sender, RoutedEventArgs e)
        {
            var connect = await boraClient.IQConnectToServer(ip, port);


            BsonMessage bsonMessage = new BsonMessage();
            bsonMessage.SetInt32Value("msg_id", 7771);


            await boraClient.SendRequestMini(bsonMessage.ToByteArray());

            boraClient.DisconnectFromServer();
        }


        private async void ButtonConnect(object sender, RoutedEventArgs e)
        {
            var connect = await boraClient.IQConnectToServer(ip, port);
            Console.WriteLine(connect);
        }

        private async void ButtonDisconnect(object sender, RoutedEventArgs e)
        {
            boraClient.DisconnectFromServer();
        }

        private async void Button_Spectrum(object sender, RoutedEventArgs e)
        {
            //var connect = await boraClient.IQConnectToServer(ip, port);

            BoraProtocol.BoraSpectrumRequest boraSpectrumRequest = new BoraProtocol.BoraSpectrumRequest()
            {
                msg_id = 7771,
                task = new BoraProtocol.TaskRequest()
                {
                    action = "start",
                    id = 2239,
                    processors = new List<string>() { "spectrum" },
                    type = "single"
                },
                spectrum = new BoraProtocol.SpectrumRequest()
                {
                    central_freq = 33 * 1e6,
                    bandwidth = 1 * 1e6,
                    filter = 1 * 1e3,
                    win_cnt = 10,
                    step_time_ms = 0,
                    start_time_ms = (long)DateTime.Now.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds,
                }
            };


            using MemoryStream ms = new MemoryStream();
            using (BsonWriter writer = new BsonWriter(ms))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(writer, boraSpectrumRequest);
            }

            //await boraClient.SendRequestMini(ms.ToArray());

            //var answer = await boraClient.GetSpectrum(); //работает

            double centralFreqHz = 15.75 * 1e6;
            double bandwidthHz = 14.25 * 1e6;

            //Запросить спектр у Боры
            //var answer2 = await boraClient.GetSpectrum(centralFreqHz, bandwidthHz, BoraProtocol.FilterSpectrum.filter_1000);

            //var answer3 = await boraClient.GetSpectrum(centralFreqHz, bandwidthHz, BoraProtocol.FilterSpectrum.filter_500);

            //var answer4 = await boraClient.GetSpectrum(centralFreqHz, bandwidthHz, BoraProtocol.FilterSpectrum.filter_100);

            var answer5 = await boraClient.GetSpectrum(centralFreqHz, bandwidthHz, 1,1);
        }

        private async void Button_WaterFall(object sender, RoutedEventArgs e)
        {
            BoraProtocol.BoraWaterFallRequest boraWaterFallRequest = new BoraProtocol.BoraWaterFallRequest()
            {
                msg_id = 7771,
                task = new BoraProtocol.TaskRequest()
                {
                    action = "start",
                    id = 2239,
                    processors = new List<string>() { "waterfall" },
                    type = "single"
                },
                waterfall = new BoraProtocol.WaterFallRequest()
                {
                    central_freq = 33 * 1e6,
                    bandwidth = 1 * 1e6,
                    filter = 1 * 1e3,
                    win_cnt = 10,
                    step_time_ms = 0,
                    start_time_ms = (long)DateTime.Now.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds,
                    end_time_ms = (long)DateTime.Now.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds + 10,
                }
            };

            var bytes = boraClient.Serialize<BoraProtocol.BoraWaterFallRequest>(boraWaterFallRequest);

            //await boraClient.SendRequestMini(bytes);

            var answer = await boraClient.GetWaterFall();
        }

        private async void Button_TimeDomain(object sender, RoutedEventArgs e)
        {

            BoraProtocol.BoraTime_DomainRequest boraTimeDomainRequest = new BoraProtocol.BoraTime_DomainRequest()
            {
                msg_id = 7771,
                task = new BoraProtocol.TaskRequest()
                {
                    action = "start",
                    id = 2239,
                    processors = new List<string>() { "time_domain" },
                    type = "single"
                },
                time_domain = new BoraProtocol.Time_DomainRequest()
                {
                    central_freq = 33 * 1e6,
                    bandwidth = 1 * 1e6,
                    sample_rate = 1 * 1e3,
                    start_time_ms = (long)DateTime.Now.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds,
                    duration_ms = 10,
                }
            };


            var bytes = boraClient.Serialize<BoraProtocol.BoraTime_DomainRequest>(boraTimeDomainRequest);

            //await boraClient.SendRequestMini(bytes);

            var answer = await boraClient.GetTimeDomain();
        }

        private async void Button_Sound(object sender, RoutedEventArgs e)
        {
            BoraProtocol.BoraSoundRequest boraSoundRequest = new BoraProtocol.BoraSoundRequest()
            {
                msg_id = 7771,
                task = new BoraProtocol.TaskRequest()
                {
                    action = "start",
                    id = 2239,
                    processors = new List<string>() { "sound" },
                    type = "single"
                },
                sound = new BoraProtocol.Sound()
                {
                    central_freq = 33 * 1e6,
                    demod_name = "FM",
                    bandwidth = 1 * 1e6,
                    sample_rate = 1 * 1e3,
                    gain_control = "auto",
                    agc_level = 1.0,
                    mgc_coef = 1.0,
                    start_time_ms = (long)DateTime.Now.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds,
                    duration_ms = 10,
                }
            };


            var bytes = boraClient.Serialize<BoraProtocol.BoraSoundRequest>(boraSoundRequest);

            //await boraClient.SendRequestMini(bytes);

            var answer = await boraClient.GetSound();
        }

        private async void Button_Control(object sender, RoutedEventArgs e)
        {
            string json = @"{
  'msg_id': 7771,
  'task': {
 'id' : 2239,
'type': 'single',
'action' : 'start',
'processors' : [
'control'
]
},
'control' : {
'att' : {
'value' : '20'
}
}
}";

            BoraProtocol.ControlRequest myDeserializedClass = new BoraProtocol.ControlRequest()
            {
                msg_id = 7771,
                task = new BoraProtocol.TaskRequest()
                {
                    action = "start",
                    id = 2239,
                    processors = new List<string>() { "control" },
                    type = "single"
                },
                control = new BoraProtocol.Control()
                {
                    devices = new List<string>() { "att" },
                    att = new BoraProtocol.Att()
                    {
                        value = "10"
                    }
                }
            };

            {
                ////var obj = JsonConvert.DeserializeObject(json);

                ////var c = BsonSerializer.Deserialize<ControlRequest>(bsonBytes);

                //var ee = boraCustomControlRequest.ToBson();

                //await boraClient.SendRequestMini(ee);
            }

            MemoryStream ms = new MemoryStream();
            using (BsonWriter writer = new BsonWriter(ms))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(writer, myDeserializedClass);
            }
            byte[] bytes = ms.ToArray();

            //await boraClient.SendRequestMini(bytes);

            var answer = await boraClient.SetControlTest();
        }

        private async void Button_Info(object sender, RoutedEventArgs e)
        {
            BoraProtocol.InfoMessageRequest infoMessageRequest = new BoraProtocol.InfoMessageRequest()
            {
                msg_id = 7771,
                task = new BoraProtocol.TaskRequest()
                {
                    action = "start",
                    id = 2239,
                    processors = new List<string>() { "info" },
                    type = "single"
                },
                info = new BoraProtocol.InfoProcessorsRequest()
                {
                    processors = new List<string>() { "sound" },
                },
            };

            //var bytes = boraClient.Serialize<InfoMessageRequest>(infoMessageRequest);

            //await boraClient.SendRequestMini(bytes);

            List<BoraProtocol.InfoProcessors> list = new List<BoraProtocol.InfoProcessors>()
                {BoraProtocol.InfoProcessors.control, BoraProtocol.InfoProcessors.control, BoraProtocol.InfoProcessors.sound};

            //var answer = await boraClient.GetInfo();

            var answer1 = await boraClient.GetInfo(new List<BoraProtocol.InfoProcessors>() { BoraProtocol.InfoProcessors.spectrum }); //???
            var answer2 = await boraClient.GetInfo(new List<BoraProtocol.InfoProcessors>() { BoraProtocol.InfoProcessors.waterfall });
            var answer3 = await boraClient.GetInfo(new List<BoraProtocol.InfoProcessors>() { BoraProtocol.InfoProcessors.time_domain });
            var answer4 = await boraClient.GetInfo(new List<BoraProtocol.InfoProcessors>() { BoraProtocol.InfoProcessors.sound });
            var answer5 = await boraClient.GetInfo(new List<BoraProtocol.InfoProcessors>() { BoraProtocol.InfoProcessors.control });
        }

        private async void Att_OnClick(object sender, RoutedEventArgs e)
        {
            var answer = await boraClient.SetAttControl(BoraProtocol.AttValues.Ten);
        }

        private async void Amp_OnClick(object sender, RoutedEventArgs e)
        {
            var answer = await boraClient.SetAmplifierControl(BoraProtocol.OnOff.@on);
        }

        private async void Com_OnClick(object sender, RoutedEventArgs e)
        {
            var answer = await boraClient.SetCommutatorSwitchControl(BoraProtocol.OnOff.@on);
        }
    }
}
