﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestBora.Example
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class TaskRequest
    {
        public int id { get; set; }
        public string type { get; set; }
        public string flow_mode { get; set; }
        public string action { get; set; }
        public List<string> processors { get; set; }
    }
    public class ExampleProcessorRequest
    {
        public int some_parameter1 { get; set; }
        public string some_parameter2 { get; set; }
    }
    public class BoraExampleRequest
    {
        public long msg_id { get; set; }
        public TaskRequest task { get; set; }
        public ExampleProcessorRequest example_processor { get; set; }
    }


    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class TaskResponce
    {
        public int id { get; set; }
        public string state { get; set; }
        public List<string> processors { get; set; }
    }

    public class DataResponce
    {
        public double some_value { get; set; }
        public string specific_answer { get; set; }
    }

    public class ExampleProcessor
    {
        public int some_parameter1 { get; set; }
        public string some_parameter2 { get; set; }
        public DataResponce data { get; set; }
    }

    public class BoraExampleResponce
    {
        public long msg_id { get; set; }
        public TaskResponce task { get; set; }
        public ExampleProcessor example_processor { get; set; }
    }



}
