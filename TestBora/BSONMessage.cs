﻿using System;
using System.Collections.Generic;

namespace Types
{
    public enum ElementType : byte
    {
        TypeDouble = 0x01,
        TypeUtf8Str = 0x02,
        TypeEmbdoc = 0x03,
        TypeArray = 0x04,
        TypeBindata = 0x05,
        TypeUndef = 0x06,
        TypeObjid = 0x07,
        TypeBool = 0x08,
        TypeDatetime = 0x09,
        TypeNull = 0x0A,
        TypeCstring = 0x0B,
        TypeDbptr = 0x0C,
        TypeJscode = 0x0D,
        TypeString = 0x0E,
        TypeJscodesc = 0x0F,
        TypeInt32 = 0x10,
        TypeTimest = 0x11,
        TypeInt64 = 0x12,
        TypeMinkey = 0xFF,
        TypeMaxkey = 0x7F
    }

    public enum BinarySubtype : byte
    {
        SubtypeNone = 0x00,
        SubtypeInt8 = 0x81,
        SubtypeInt16 = 0x82,
        SubtypeInt32 = 0x83,
        SubtypeInt64 = 0x84,
        SubtypeFloat = 0x90,
        SubtypeDoube = 0x91,
        SubtypeIqfloat = 0xA0,
        SubtypeIqdoube = 0xA1,
        SubtupeIqshort = 0xA2
    }

    public class BsonElement
    {
        public int Size;
        public ElementType Type;
        public BinarySubtype Subtype;
        public string Key;
        public Object Value;

        public int raw_size()
        {
            int result = 1 /* place for type */ + Key.Length + 1 /* place for 0x00 of key */ + Size;
            if (Type == ElementType.TypeBindata)
            {
                result += 4 + 1; // LEN + SUBTYPE
            }
            return result;
        }

        public byte GetTypeByte()
        {
            return (byte)Type;
        }

        public byte[] GetKeyBytes()
        {
            var result = new byte[Key.Length + 1];
            for (int i = 0; i < Key.Length; i++)
            {
                result[i] = (byte)Key[i];
            }
            result[result.Length - 1] = 0;
            return result;
        }

        public override string ToString()
        {
            var result = String.Empty;
            switch (Type)
            {
                case ElementType.TypeDouble:
                    result = ((double) Value).ToString();
                    break;
                case ElementType.TypeUtf8Str:
                    break;
                case ElementType.TypeEmbdoc:
                    break;
                case ElementType.TypeArray:
                    result = $"Array with len {(Array) Value}".Length.ToString();
                    break;
                case ElementType.TypeBindata:
                    result = "BinData";
                    break;
                case ElementType.TypeUndef:
                    break;
                case ElementType.TypeObjid:
                    break;
                case ElementType.TypeBool:
                    result = ((bool) Value).ToString();
                    break;
                case ElementType.TypeDatetime:
                    break;
                case ElementType.TypeNull:
                    break;
                case ElementType.TypeCstring:
                    break;
                case ElementType.TypeDbptr:
                    break;
                case ElementType.TypeJscode:
                    break;
                case ElementType.TypeString:
                    return (string) Value;
                    break;
                case ElementType.TypeJscodesc:
                    break;
                case ElementType.TypeInt32:
                    result = ((int) Value).ToString();
                    break;
                case ElementType.TypeTimest:
                    break;
                case ElementType.TypeInt64:
                    result = ((long) Value).ToString();
                    break;
                case ElementType.TypeMinkey:
                    break;
                case ElementType.TypeMaxkey:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return result;
        }
    }

    public class BsonMessage
    {
        private readonly Dictionary<string, BsonElement> _elements;

        public BsonMessage()
        {
            _elements = new Dictionary<string, BsonElement>();
        }

        private int raw_size()
        {
            int result = 4 + 1; // size and 0x0 at the end

            foreach (var element in _elements.Values)
            {
                result += element.raw_size();
            }

            return result;
        }

        public string[] GetKeyList()
        {
            var keyList = new List<string>(_elements.Keys);
            return keyList.ToArray();
        }

        public bool HasField(string key)
        {
            return _elements.ContainsKey(key);
        }
               
        public BsonElement GetElement(string Key)
        {
            if (HasField(Key))
            {
                return _elements[Key];
            }

            return null;
        }


        public int GetInt32Value(string key)
        {
            BsonElement e;
            if (_elements.TryGetValue(key, out e))
            {
                return (int)e.Value;
            }
            else
            {
                return -1;
            }
        }

        public void SetInt32Value(string newkey, int newvalue)
        {
            var e = new BsonElement { Value = newvalue, Key = newkey, Type = ElementType.TypeInt32, Size = sizeof(int) };
            _elements.Add(newkey, e);
        }

        public long GetInt64Value(string key)
        {
            BsonElement e;
            if (_elements.TryGetValue(key, out e))
            {
                return (long)e.Value;
            }
            else
            {
                return -1;
            }
        }

        public void SetInt64Value(string newkey, long newvalue)
        {
            var e = new BsonElement { Value = newvalue, Key = newkey, Type = ElementType.TypeInt64, Size = sizeof(long) };
            _elements.Add(newkey, e);
        }

        public double GetDoubleValue(string key)
        {
            BsonElement e;
            if (_elements.TryGetValue(key, out e))
            {
                return (double)e.Value;
            }
            return Double.NaN;
        }

        public void SetDoubleValue(string newkey, double newvalue)
        {
            var e = new BsonElement { Key = newkey, Value = newvalue, Type = ElementType.TypeDouble, Size = sizeof(double) };
            _elements.Add(newkey, e);
        }

        public byte[] GetBinaryValue(string key)
        {
            BsonElement e;
            if (_elements.TryGetValue(key, out e))
            {
                return (byte[])e.Value;
            }
            return null;
        }

        public BinarySubtype GetBinarySubtype(string key)
        {
            BinarySubtype res = BinarySubtype.SubtypeNone;

            BsonElement e;
            if (_elements.TryGetValue(key, out e))
            {
                return e.Subtype;
            }
            return res;
        }

        public void SetBinaryValue(string newkey, byte[] newvalue)
        {
            if (newvalue == null)
            {
                throw new ArgumentNullException("SetBinaryValue");
            }
            byte[] tmpBuf = newvalue;
            var e = new BsonElement { Value = tmpBuf, Key = newkey, Type = ElementType.TypeBindata, Size = tmpBuf.Length };
            _elements.Add(newkey, e);
        }

        public void FromByteArray(byte[] readBuf)
        {
            if (readBuf == null)
            {
                throw new ArgumentNullException("FromByteArray");
            }
            int bufPosition = 0;
            int bufLimit = readBuf.Length;
            while (bufPosition < bufLimit - 1)
            {
                byte type = readBuf[bufPosition];
                int size = 0;
                BinarySubtype subtype = 0;
                bufPosition++;
                string fieldName = string.Empty;
                while (readBuf[bufPosition] != 0)
                {
                    fieldName += Convert.ToChar(readBuf[bufPosition]);
                    bufPosition++;
                }
                bufPosition++;
                if (type == (byte)ElementType.TypeBindata)
                {
                    size = BitConverter.ToInt32(readBuf, bufPosition);
                    bufPosition += 4;
                    subtype = (BinarySubtype)readBuf[bufPosition];
                    bufPosition++;
                }

                var element = new BsonElement();
                var etype = (ElementType)type;
                switch (etype)
                {
                    case ElementType.TypeDouble:
                        element.Key = fieldName;
                        element.Size = sizeof(double);
                        element.Value = BitConverter.ToDouble(readBuf, bufPosition);
                        element.Type = etype;
                        _elements.Add(fieldName, element);
                        break;

                    case ElementType.TypeInt32:
                        element.Key = fieldName;
                        element.Size = sizeof(int);
                        element.Value = BitConverter.ToInt32(readBuf, bufPosition);
                        element.Type = etype;
                        _elements.Add(fieldName, element);
                        break;

                    case ElementType.TypeBindata:
                        byte[] newvalue = new byte[size];
                        Buffer.BlockCopy(readBuf, bufPosition, newvalue, 0, size);
                        element.Key = fieldName;
                        element.Size = size;
                        element.Value = newvalue;
                        element.Subtype = subtype;
                        element.Type = etype;
                        _elements.Add(fieldName, element);
                        break;

                    case ElementType.TypeInt64:
                        element.Key = fieldName;
                        element.Size = sizeof(long);
                        element.Value = BitConverter.ToInt64(readBuf, bufPosition);
                        element.Type = etype;
                        _elements.Add(fieldName, element);
                        break;
                }
                bufPosition += element.Size;
            }
        }

        public byte[] ToByteArray()
        {
            var resultSize = raw_size();
            var result = new byte[resultSize];
            int resultPos = 0;

            Buffer.BlockCopy(BitConverter.GetBytes(resultSize), 0, result, 0, sizeof(Int32));
            resultPos += sizeof(Int32);

            foreach (BsonElement e in _elements.Values)
            {
                result[resultPos] = e.GetTypeByte();
                resultPos++;

                var keyStr = e.GetKeyBytes();
                Buffer.BlockCopy(keyStr, 0, result, resultPos, e.Key.Length);
                resultPos += keyStr.Length;

                switch (e.Type)
                {
                    case ElementType.TypeDouble:
                        {
                            Buffer.BlockCopy(BitConverter.GetBytes((double)e.Value), 0, result, resultPos, sizeof(double));
                            resultPos += sizeof(double);
                        }
                        break;

                    case ElementType.TypeInt32:
                        {
                            Buffer.BlockCopy(BitConverter.GetBytes((Int32)e.Value), 0, result, resultPos, sizeof(Int32));
                            resultPos += sizeof(Int32);
                        }
                        break;

                    case ElementType.TypeInt64:
                        {
                            Buffer.BlockCopy(BitConverter.GetBytes((Int64)e.Value), 0, result, resultPos, sizeof(Int64));
                            resultPos += sizeof(Int64);
                        }
                        break;

                    case ElementType.TypeBindata:
                        {
                            Buffer.BlockCopy(BitConverter.GetBytes(e.Size), 0, result, resultPos, sizeof(Int32));
                            resultPos += sizeof(Int32);
                            result[resultPos] = (byte)e.Subtype;
                            resultPos++;
                            Buffer.BlockCopy((byte[])e.Value, 0, result, resultPos, e.Size);
                            resultPos += e.Size;
                        }
                        break;
                }
            }
            return result;
        }
    }
}