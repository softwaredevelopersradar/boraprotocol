﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoraClientDll
{
    public partial class BoraProtocol
    {
        public class BoraSoundRequest : IBinarySerializable
        {
            public long msg_id { get; set; }
            public TaskRequest task { get; set; }
            public Sound sound { get; set; }
        }

        public class BoraSoundResponce : IBinarySerializable
        {
            public long msg_id { get; set; }
            public TaskResponce task { get; set; }
            public SoundResponce sound { get; set; }
        }

        public class Sound
        {
            public double central_freq { get; set; } // центральная частота Гц
            public string demod_name { get; set; } //Тип демодулятора: “AM”, “FM”, “USB”, “LSB”, … Возможные типы демодуляторов необходимо получить у сервера в info
            public double bandwidth { get; set; } // Полоса демодулятора (где применимы различные полосы) 
            public double sample_rate { get; set; } // Желаемая частота дискретизации Гц
            public string gain_control { get; set; } // Способ управления регулировкой уровня: •	автоматический = “auto” •	ручной = “manual” Возможные типы РУ необходимо получить у сервера в info
            public double agc_level { get; set; } //Уровень АРУ (0.0 … 1.0) 
            public double mgc_coef { get; set; } //Коэфициент РРУ 
            public long start_time_ms { get; set; } // Момент начала сигнала Дата и время начала чего-либо. Задаётся в миллисекундах(мс), считая от 1970-01-01 00:00:00.000 GMT+0
            public long duration_ms { get; set; } // Длительность сигнала мс
        }

        public class SoundRequest
        {
            public double central_freq { get; set; } // центральная частота Гц
            public string demod_name { get; set; } //Тип демодулятора: “AM”, “FM”, “USB”, “LSB”, … Возможные типы демодуляторов необходимо получить у сервера в info
            public double bandwidth { get; set; } // Полоса демодулятора (где применимы различные полосы) 
            public double sample_rate { get; set; } // Желаемая частота дискретизации Гц
            public string gain_control { get; set; } // Способ управления регулировкой уровня: •	автоматический = “auto” •	ручной = “manual” Возможные типы РУ необходимо получить у сервера в info
            public double agc_level { get; set; } //Уровень АРУ (0.0 … 1.0) 
            public double mgc_level { get; set; } //Коэфициент РРУ 
            public long start_time_ms { get; set; } // Момент начала сигнала Дата и время начала чего-либо. Задаётся в миллисекундах(мс), считая от 1970-01-01 00:00:00.000 GMT+0
            public long duration_ms { get; set; } // Длительность сигнала мс
        }

        public class SoundResponce
        {
            public byte[] audio_data { get; set; }
            public double central_freq { get; set; } // центральная частота Гц
            public string demod_name { get; set; } //Тип демодулятора: “AM”, “FM”, “USB”, “LSB”, … Возможные типы демодуляторов необходимо получить у сервера в info
            public double bandwidth { get; set; } // Полоса демодулятора (где применимы различные полосы) 
            public double sample_rate { get; set; } // Желаемая частота дискретизации Гц
            public string gain_control { get; set; } // Способ управления регулировкой уровня: •	автоматический = “auto” •	ручной = “manual” Возможные типы РУ необходимо получить у сервера в info
            public double agc_level { get; set; } //Уровень АРУ (0.0 … 1.0) 
            public double mgc_level { get; set; } //Коэфициент РРУ 
            public long start_time_ms { get; set; } // Момент начала сигнала Дата и время начала чего-либо. Задаётся в миллисекундах(мс), считая от 1970-01-01 00:00:00.000 GMT+0
            public long duration_ms { get; set; } // Длительность сигнала мс
        }

    }
}
