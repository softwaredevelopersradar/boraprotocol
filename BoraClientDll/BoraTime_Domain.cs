﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoraClientDll
{
    public partial class BoraProtocol
    {
        public class BoraTime_DomainRequest : IBinarySerializable
        {
            public long msg_id { get; set; }
            public TaskRequest task { get; set; }
            public Time_DomainRequest time_domain { get; set; }
        }

        public class BoraTime_DomainResponce : IBinarySerializable
        {
            public long msg_id { get; set; }
            public TaskResponce task { get; set; }
            public Time_DomainResponce time_domain { get; set; }
        }

        public class Time_DomainRequest
        {
            public double central_freq { get; set; } // центральная частота Гц
            public double bandwidth { get; set; } // Желаемая полоса  Гц
            public double sample_rate { get; set; } // Желаемая частота дискретизации Гц
            public long start_time_ms { get; set; } // Момент начала сигнала Дата и время начала чего-либо. Задаётся в миллисекундах(мс), считая от 1970-01-01 00:00:00.000 GMT+0
            public long duration_ms { get; set; } // Длительность сигнала мс
        }

        public class Time_DomainResponce
        {
            public byte[] raw_iq { get; set; } //Массив IQ, тип данных указан в BSON Тип данных выбирает сервер 
            public double central_freq { get; set; } // Центральная частота.             Может отличаться от запрошенной клиентом.Обычно сервером обеспечивается возможность указания частоты с точностью 1 Гц для полос до 100 кГц
            public double bandwidth { get; set; } // Полоса пропускания фильтра.             Может отличаться от запрошенной клиентом в большую сторону
            public double sample_rate { get; set; } // Частота дискретизации.    Может отличаться от запрошенной клиентом в большую или меньшую сторону.
            public double terr_val_mks { get; set; }
            public double terr_speed_mks_ps { get; set; }
            public long start_time_ms { get; set; } // Момент начала сигнала Дата и время начала чего-либо. Задаётся в миллисекундах(мс), считая от 1970-01-01 00:00:00.000 GMT+0
            public long duration_ms { get; set; } // Длительность сигнала мс
        }

        public class Time_DomainResponce2 : Time_DomainRequest
        {
            public byte[] raw_iq { get; set; } //Массив IQ, тип данных указан в BSON Тип данных выбирает сервер 
            public double terr_val_mks { get; set; }
            public double terr_speed_mks_ps { get; set; }
        }

    }
}
