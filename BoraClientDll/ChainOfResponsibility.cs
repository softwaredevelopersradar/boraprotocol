﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoraClientDll
{
    using System.IO;
    using System.Threading.Tasks;

    using NLog;

    public partial class BoraClient
    {
        private Handler h1;
        private Handler h2;
        private Handler h3;
        private Handler h4;
        private Handler h5;
        private Handler h6;
        private Handler h7;
        private Handler h8;
        private Handler h9;


        public void InitChain()
        {
            h1 = new SpectrumHandler();
            h2 = new WaterFallHandler();
            h3 = new TimeDomainHandler();
            h4 = new SoundHandler();
            h5 = new ControlHandler();
            h6 = new InfoHandler();
            h7 = new ErrorHandler();
            h8 = new TaskResponseHandler();
            h9 = new ObjectHandler();

            h1.Successor = h2;
            h2.Successor = h3;
            h3.Successor = h4;
            h4.Successor = h5;
            h5.Successor = h6;
            h6.Successor = h7;
            h7.Successor = h8;
            this.h8.Successor = this.h9;
        }

        public Task<IBinarySerializable> StartChain(BoraClientDll.BoraClient boraClient, MemoryStream bytes)
        {
            return h1.HandleRequest(boraClient, bytes);
        }

        abstract class Handler
        {
            protected Handler()
            {
                this.logger = LogManager.GetCurrentClassLogger();
            }
            public ILogger logger;
            public Handler Successor { get; set; }
            public abstract Task<IBinarySerializable> HandleRequest(BoraClientDll.BoraClient boraClient, MemoryStream bytes);
        }

        class SpectrumHandler : Handler
        {
            
            public override async Task<IBinarySerializable> HandleRequest(BoraClientDll.BoraClient boraClient, MemoryStream bytes)
            {
                // некоторая обработка запроса

                var deserialize = boraClient.Deserialize<BoraProtocol.BoraSpectrumResponce>(bytes);

                if (deserialize?.spectrum != null)
                {
                    // завершение выполнения запроса;
                    return deserialize;
                }
                // передача запроса дальше по цепи при наличии в ней обработчиков
                else
                {
                    return await (this.Successor?.HandleRequest(boraClient, bytes)).ConfigureAwait(false);
                }
            }
        }

        class WaterFallHandler : Handler
        {
            public override async Task<IBinarySerializable> HandleRequest(BoraClientDll.BoraClient boraClient, MemoryStream bytes)
            {
                // некоторая обработка запроса
                var deserialize = boraClient.Deserialize<BoraProtocol.BoraWaterFallResponce>(bytes);

                if (deserialize?.waterfall != null)
                {
                    // завершение выполнения запроса;
                    return deserialize;
                }
                // передача запроса дальше по цепи при наличии в ней обработчиков
                else
                {
                    return await (this.Successor?.HandleRequest(boraClient, bytes)).ConfigureAwait(false);
                }
            }
        }

        class TimeDomainHandler : Handler
        {
            public override async Task<IBinarySerializable> HandleRequest(BoraClientDll.BoraClient boraClient, MemoryStream bytes)
            {
                // некоторая обработка запроса
                var deserialize = boraClient.Deserialize<BoraProtocol.BoraTime_DomainResponce>(bytes);

                if (deserialize?.time_domain != null)
                {
                    // завершение выполнения запроса;
                    return deserialize;
                }
                // передача запроса дальше по цепи при наличии в ней обработчиков
                else
                {
                    return await (this.Successor?.HandleRequest(boraClient, bytes)).ConfigureAwait(false);
                }
            }
        }

        class SoundHandler : Handler
        {
            public override async Task<IBinarySerializable> HandleRequest(BoraClientDll.BoraClient boraClient, MemoryStream bytes)
            {
                // некоторая обработка запроса
                var deserialize = boraClient.Deserialize<BoraProtocol.BoraSoundResponce>(bytes);

                if (deserialize?.sound != null)
                {
                    // завершение выполнения запроса;
                    return deserialize;
                }
                // передача запроса дальше по цепи при наличии в ней обработчиков
                else
                {
                    return await (this.Successor?.HandleRequest(boraClient, bytes)).ConfigureAwait(false);
                }
            }
        }

        class ControlHandler : Handler
        {
            public override async Task<IBinarySerializable> HandleRequest(BoraClientDll.BoraClient boraClient, MemoryStream bytes)
            {
                // некоторая обработка запроса

                //var deserialize00 = boraClient.Deserialize<object>(bytes);
                var deserialize = boraClient.Deserialize<BoraProtocol.ControlResponce>(bytes);

                if (deserialize?.control != null)
                {
                    // завершение выполнения запроса;
                    return deserialize;
                }
                // передача запроса дальше по цепи при наличии в ней обработчиков
                else
                {
                    return await (this.Successor?.HandleRequest(boraClient, bytes)).ConfigureAwait(false);
                }
            }
        }

        class InfoHandler : Handler
        {
            public override async Task<IBinarySerializable> HandleRequest(BoraClientDll.BoraClient boraClient, MemoryStream bytes)
            {
                // некоторая обработка запроса

                //var deserialize0 = boraClient.Deserialize<object>(bytes);
                var deserialize = boraClient.Deserialize<BoraProtocol.InfoMessageResponce>(bytes);

                if (deserialize?.info != null)
                {
                    // завершение выполнения запроса;
                    return deserialize;
                }
                // передача запроса дальше по цепи при наличии в ней обработчиков
                else
                {
                    return await (this.Successor?.HandleRequest(boraClient, bytes)).ConfigureAwait(false);
                }
            }
        }

        class ErrorHandler : Handler
        {
            public override async Task<IBinarySerializable> HandleRequest(BoraClientDll.BoraClient boraClient, MemoryStream bytes)
            {
                // некоторая обработка запроса

                //var deserialize0 = boraClient.Deserialize<object>(bytes);
                BoraProtocol.ErrorResponce deserializeError = boraClient.Deserialize<BoraProtocol.ErrorResponce>(bytes);

                if (deserializeError?.error != null)
                {
                    // завершение выполнения запроса;
                    this.logger.Error($"Error! Task id: {deserializeError.task.id} is {deserializeError.task.state}");
                    this.logger.Error($"Error code: {deserializeError.error.code} comment: {deserializeError.error.comment}");

                    // завершение выполнения запроса;
                    return deserializeError;
                }
                // передача запроса дальше по цепи при наличии в ней обработчиков
                else
                {
                    return await (this.Successor?.HandleRequest(boraClient, bytes)).ConfigureAwait(false);
                }
            }
        }

        class TaskResponseHandler : Handler
        {
            /// <inheritdoc />
            public override async Task<IBinarySerializable> HandleRequest(BoraClient boraClient, MemoryStream bytes)
            {
                var deserialize0 = boraClient.Deserialize<BoraProtocol.DefaultTask>(bytes);

                if (deserialize0?.task != null)
                {
                    this.logger.Trace(deserialize0);
                    return deserialize0;
                }
                // передача запроса дальше по цепи при наличии в ней обработчиков
                else
                {
                    throw new ArgumentException("Error while parsing message!");
                }
            }
        }

        class ObjectHandler : Handler
        {
            public override async Task<IBinarySerializable> HandleRequest(BoraClientDll.BoraClient boraClient, MemoryStream bytes)
            {
                // некоторая обработка запроса

                var deserialize0 = boraClient.Deserialize<object>(bytes);

                if (deserialize0 != null)
                {
                    this.logger.Trace(deserialize0);
                    return null;
                }
                // передача запроса дальше по цепи при наличии в ней обработчиков
                else
                {
                    throw new ArgumentException("Error while parsing message!");
                }
            }
        }
    }

}