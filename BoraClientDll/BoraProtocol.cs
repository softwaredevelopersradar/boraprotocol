﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace BoraClientDll
{
    public partial class BoraProtocol
    {
        public class TaskRequest
        {
            public long id { get; set; }
            public string type { get; set; }
            public string action { get; set; }
            public string flow_mode { get; set; }
            public List<string> processors { get; set; }
        }

        public class DefaultTask : IBinarySerializable
        {
            public long msg_id { get; set; }
            public TaskResponce task { get; set; }
        }
        public class TaskResponce
        {
            public long id { get; set; }
            public string state { get; set; }
            public List<string> processors { get; set; }
        }

        public class ErrorResponce : IBinarySerializable
        {
            public long msg_id { get; set; }
            public TaskResponce task { get; set; }
            public Error error { get; set; }
        }

        public class Error
        {
            public int code { get; set; }
            public string comment { get; set; }
        }
    }
}
