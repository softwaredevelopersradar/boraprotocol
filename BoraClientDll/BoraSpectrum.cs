﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoraClientDll
{
    public partial class BoraProtocol
    {
        public class BoraSpectrumRequest : IBinarySerializable
        {
            public long msg_id { get; set; }
            public TaskRequest task { get; set; }
            public SpectrumRequest spectrum { get; set; }
        }

        public class SpectrumRequest
        {
            public double central_freq { get; set; } // центральная частота Гц
            public double bandwidth { get; set; } // полоса Гц
            public double filter { get; set; } // Фильтр преобразования Фурье Гц . В ответе будет bandwidth/filter точек для каждого спектра.
            public int win_cnt { get; set; } // Кол-во «окон усреднения». Происходит накопление сигнала длительностью win_cnt/filter секунд Штуки
            public long step_time_ms { get; set; } // Размер шага, мс. Параметр временного шага по сигналу для расчета спектра. Имеет смысл в запросах типа старт-стоп. Спектры будут рассчитываться сервером каждые step_time_ms, начиная с момента start_time_ms 
            public long start_time_ms { get; set; } // Момент начала сигнала Дата и время начала чего-либо. Задаётся в миллисекундах(мс), считая от 1970-01-01 00:00:00.000 GMT+0
        }


        public class BoraSpectrumResponce : IBinarySerializable
        {
            public long msg_id { get; set; }
            public TaskResponce task { get; set; }
            public SpectrumResponce spectrum { get; set; }
        }
        public class SpectrumResponce
        {
            public byte[] powers { get; set; } //Массив амплитуд, тип данных указан в BSON
            public double scale_coef { get; set; } // Коэффициент, на который клиенту необходимо умножить значения из массива powers 
            public double central_freq { get; set; } // центральная частота Гц
            public double bandwidth { get; set; } // полоса Гц
            public double filter { get; set; } // Фильтр преобразования Фурье Гц . В ответе будет bandwidth/filter точек для каждого спектра.
            public int win_cnt { get; set; } // Кол-во «окон усреднения». Происходит накопление сигнала длительностью win_cnt/filter секунд Штуки
            public long start_time_ms { get; set; } // Момент начала сигнала Дата и время начала чего-либо. Задаётся в миллисекундах(мс), считая от 1970-01-01 00:00:00.000 GMT+0
            public long end_time_ms { get; set; } //Момент окончания сигнала. Например, в случае, если filter = 1 кГц, win_cnt = 10, то в ответе end_time_ms будет равен start_time_ms + 10 мс.
        }
    }
}
