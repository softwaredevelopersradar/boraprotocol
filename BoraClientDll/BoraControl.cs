﻿using System.Collections.Generic;

namespace BoraClientDll
{
    public partial class BoraProtocol
    {
        public class ControlRequest : IBinarySerializable
        {
            public long msg_id { get; set; }
            public BoraProtocol.TaskRequest task { get; set; }
            public Control control { get; set; }
        }

        public class ControlResponce : IBinarySerializable
        {
            public long msg_id { get; set; }
            public BoraProtocol.TaskResponce task { get; set; }
            public Control control { get; set; }
        }

        public class Att
        {
            public string value { get; set; }
        }
        public class Att2 : Att
        {
            public List<string> value_list { get; set; } = new List<string>() { "0", "10", "20", "30" };
        }
        public class Att3 : Att
        {
            public List<string> value_list { get; set; }
        }
        public class Control
        {
            public List<string> devices { get; set; }
            public Att att { get; set; }
        }

        public class Device
        {
            public string value { get; set; }
        }
        public class Control2
        {
            public Device att { get; set; }
            public Device amplifier { get; set; }
            public Device commutator_switch { get; set; }
        }
    }
}
