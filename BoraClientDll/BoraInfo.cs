﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoraClientDll
{
    public partial class BoraProtocol
    {
        public class InfoMessageRequest : IBinarySerializable
        {
            public long msg_id { get; set; }
            public TaskRequest task { get; set; }
            public InfoProcessorsRequest info { get; set; }
        }

        public class InfoProcessorsRequest
        {
            public List<string> processors { get; set; }
        }

        public class InfoProcessorsResponce
        {
            public List<string> processors { get; set; }
            public InfoSpectrum spectrum { get; set; }
            public InfoWaterFall waterfall { get; set; }
            public InfoTime_Domain time_domain { get; set; }
            public InfoSound sound { get; set; }
            public InfoControlResponce control { get; set; }
        }

        public class InfoSpectrum
        {
            public double min_freq { get; set; }  //Значение минимально возможной частоты
            public double max_freq { get; set; }  //Значение максимально возможной частоты
            public double max_bandwidth { get; set; }  //Максимально возможная полоса
            public double[] filter_list { get; set; }  //Массив возможных фильтров. Пустой, если возможны любые фильтры
            public int[] win_cnt_list { get; set; }  //Массив возможных win_cnt
        }

        public class InfoWaterFall
        {
            public double min_freq { get; set; }  //Значение минимально возможной частоты
            public double max_freq { get; set; }  //Значение максимально возможной частоты
            public double max_bandwidth { get; set; }  //Максимально возможная полоса
            public double[] filter_list { get; set; }  //Массив возможных фильтров. Пустой, если возможны любые фильтры
            public int[] win_cnt_list { get; set; }  //Массив возможных win_cnt
        }

        public class InfoTime_Domain
        {
            public double min_freq { get; set; }  //Значение минимально возможной частоты
            public double max_freq { get; set; }  // Значение максимально возможной частоты
            public double[] bandwidth_list { get; set; }  // Массив возможных полос.
            public double[] sample_rate_list { get; set; }  //   Массив возможных частот дискретизации. 

        }

        public class InfoSound
        {
            public double min_freq { get; set; }  //Значение минимально возможной частоты
            public double max_freq { get; set; }  //Значение максимально возможной частоты
            public string[] demod_name_list { get; set; }  //Список возможных демодуляторов(строки). Например: [“AM”, “FM”, “LSB”, “USB”]
            public string[] gain_control_list { get; set; }  //Список возможных способов регулировки уровня.Например: [“auto”, “manual”]
            public double[] bandwidth_list { get; set; }  //Массив возможных полос для демодуляторов.
            public double[] sample_rate_list { get; set; }  //Массив возможных частот дискретизации.
        }

        public class InfoControl
        {
            public string value { get; set; }  //Текущее значение 
            public string[] value_list { get; set; }  //Список возможных значений 
        }

        public class InfoControlResponce
        {
            public List<string> devices { get; set; } 
            public Att3 att { get; set; }
        }


        public class InfoMessageResponce : IBinarySerializable
        {
            public long msg_id { get; set; }
            public TaskResponce task { get; set; }
            public InfoProcessorsResponce info { get; set; }
        }

    }
}
