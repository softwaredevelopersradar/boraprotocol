﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Nito.AsyncEx;
using static BoraClientDll.BoraProtocol;

namespace BoraClientDll
{
    using NLog;

    public partial class BoraClient
    {
        private TcpClient client;

        private ILogger logger = LogManager.GetCurrentClassLogger();
        private string IP = "127.0.0.1";
        private int port = 6789;

        bool disconnect = false;

        private readonly AsyncLock asyncLock;

        //private readonly ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>> concurrentDictionary;

        public delegate void IsConnectedEventHandler(bool isConnected);
        public event IsConnectedEventHandler IsConnected;

        public delegate void OnReadEventHandler(bool isRead);
        public event OnReadEventHandler IsRead;

        public delegate void IsWriteEventHandler(bool isWrite);
        public event IsWriteEventHandler IsWrite;

        public CancellationTokenSource ReadTaskTokenSource { get; set; }
        public async Task<ConnectionStatus> IQConnectToServer(string IP, int port, int timeout = 3000, CancellationToken token = default)
        {
            Regex regex = new Regex(@"^(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])){3}$");
            if (regex.IsMatch(IP, 0))
            {

                var task = ConnectToServerWCancelToken(IP, port, token);
                await Task.WhenAny(task, Task.Delay(timeout, token)).ConfigureAwait(false);
                var connectionResult = await task.ConfigureAwait(false);
                if (connectionResult)
                {
                    this.logger.Trace($"Connected to bora {IP}:{port}");
                    return ConnectionStatus.Connected;
                }
                else
                {
                    this.logger.Warn("Error due connection to bora server.");
                    return ConnectionStatus.ServerNotResponding;
                }
            }
            else
            {
                return ConnectionStatus.InvalidIp;
            }
        }
        private async Task<bool> ConnectToServerWCancelToken(string IP, int port, CancellationToken token)
        {
            disconnect = false;
            client = new TcpClient();

            while (!client.Connected)
            {
                if (token.IsCancellationRequested)
                    return false;
                try
                {
#if NET5_0
                    await client.ConnectAsync(IPAddress.Parse(IP), port, token).ConfigureAwait(false);
#else
                    await client.ConnectAsync(IPAddress.Parse(IP), port).ConfigureAwait(false);
#endif

                    this.IP = IP;
                    this.port = port;
                }
                catch (Exception e)
                {
                    this.logger.Error($"Error while connecting to server {e.Message}");
                }

                if (!client.Connected)
                    return false;
            }
            IsConnected?.Invoke(true);
            this.ReadTaskTokenSource = new CancellationTokenSource(); 
            return true;
        }

        public void DisconnectFromServer()
        {
            disconnect = true;
            client?.Close();
            IsConnected?.Invoke(false);
        }

        private async Task<IBinarySerializable> ReadMessage(TcpClient client,CancellationToken token)
        {

            byte[] data = new byte[65355];
            NetworkStream stream = client.GetStream();

            using (MemoryStream ms = new MemoryStream(this.client.ReceiveBufferSize * 10))
            {
                do
                {
                    do
                    {
                        int bytes = 0;
                        bytes = await stream.ReadAsync(data, 0, data.Length, token).ConfigureAwait(false);
                        await ms.WriteAsync(data, 0, bytes, token).ConfigureAwait(false);
                        IsRead?.Invoke(true);

                    }
                    while (stream.DataAvailable && !token.IsCancellationRequested);
                    if (ms.Length > 1200000)
                    {
                        //largest bora package.
                        break;
                    }
                       
                }
                while (this .Deserialize<object>(ms.ToArray()) == null
                       && !token.IsCancellationRequested); // пока данные есть в потоке
                return await StartChain(this, ms); ;
            }
        }

        public BoraClient()
        {
            asyncLock = new AsyncLock();
            InitChain();
        }

        //Просто отправить
        public async Task SendRequestMini(byte[] message, CancellationToken token = default)
        {
            using (await asyncLock.LockAsync(token).ConfigureAwait(false))
            {
                int count = message.Count();
                try
                {
                    await client.GetStream().WriteAsync(message, 0, count, token).ConfigureAwait(false);
                    IsWrite?.Invoke(true);
                }
                catch (Exception)
                {
                    IsWrite?.Invoke(false);
                }
            }
        }

        public byte[] Serialize<T>(T obj)
        {
            using (MemoryStream ms = new MemoryStream())
            using (BsonWriter writer = new BsonWriter(ms))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(writer, obj);
                return ms.ToArray();
            }
        }

        public T Deserialize<T>(byte[] bytes)
        {
            using (MemoryStream ms = new MemoryStream(bytes))
            using (BsonReader reader = new BsonReader(ms))
            {
                JsonSerializer serializer = new JsonSerializer();
                try
                {
                    var deserialize = serializer.Deserialize<T>(reader);
                    return deserialize;
                }
                catch (Exception exception)
                {
                    return default(T);
                }
            }
        }

        public T Deserialize<T>(MemoryStream  ms)
        {
            ms.Position = 0;
            using (BsonReader reader = new BsonReader(ms))
            {
                reader.CloseInput = false;
                JsonSerializer serializer = new JsonSerializer();
                try
                {
                    return serializer.Deserialize<T>(reader);
                }
                catch (Exception exception)
                {
                    return default(T);
                }
            }
        }

        private async Task<IBinarySerializable> SendRequest(TcpClient client, int requestCode, byte[] message, CancellationToken token)
        {
            int count = message.Count();

            using (await this.asyncLock.LockAsync(token).ConfigureAwait(false))
            {
                try
                {
                    await client.GetStream().FlushAsync(token).ConfigureAwait(false);
                    await client.GetStream().WriteAsync(message, 0, count, token).ConfigureAwait(false);
                    IsWrite?.Invoke(true);
                    return await this.ReadMessage(client,token).ConfigureAwait(false);
                }
                catch (ArgumentException ex)
                {
                    this.logger.Warn("Error while parsing data");
                    return null;
                }
                catch (Exception e)
                {
                    this.logger.Error(string.Format("Error due request handle {0}", e.Message));
                }

                return null;
            }
        }


        private async Task SendRequestWithoutAnswer(TcpClient client, int requestCode, byte[] message, CancellationToken token)
        {
            int count = message.Count();

            using (await this.asyncLock.LockAsync(token).ConfigureAwait(false))
            {
                try
                {
                    await client.GetStream().FlushAsync(token).ConfigureAwait(false);
                    await client.GetStream().WriteAsync(message, 0, count, token).ConfigureAwait(false);
                    IsWrite?.Invoke(true);
                }
                catch (ArgumentException ex)
                {
                    this.logger.Warn("Error while parsing data");
                }
                catch (Exception e)
                {
                    this.logger.Error(string.Format("Error due request handle {0}", e.Message));
                }
            }
        }




        private static long _msg_id = 1;

        private static long _spectrumTaskId = 1_000_000_000_000_000_000;
        private static long _waterFallTaskId = 2_000_000_000_000_000_000;
        private static long _timeDomainTaskId = 3_000_000_000_000_000_000;
        private static long _soundTaskId = 4_000_000_000_000_000_000;
        private static long _controlTaskId = 5_000_000_000_000_000_000;
        private static long _infoTaskId = 6_000_000_000_000_000_000;


        private enum RequestsCodes
        {
            SpectrumRequest = 1,
            WaterFallRequest = 2,
            TimeDomainRequest = 3,
            SoundRequest = 4,
            ControlRequest = 5,
            InfoRequest = 6,
        }

        private enum ResponceCodes
        {
            SpectrumResponce = 1,
            WaterFallResponce = 2,
            TimeDomainResponce = 3,
            SoundResponce = 4,
            ControlResponce = 5,
            InfoResponce = 6,
        }

        private static int GetCode(RequestsCodes requestsCode)
        {
            return (int) requestsCode;
        }

        private static int GetCode(ResponceCodes responceCode)
        {
            return (int)responceCode;
        }

        private static int GetCode(IBinarySerializable iBinarySerializable)
        {
            if (iBinarySerializable is BoraProtocol.BoraSpectrumRequest || iBinarySerializable is BoraProtocol.BoraSpectrumResponce) return 1;
            if (iBinarySerializable is BoraProtocol.BoraWaterFallRequest || iBinarySerializable is BoraProtocol.BoraWaterFallResponce) return 2;
            if (iBinarySerializable is BoraProtocol.BoraTime_DomainRequest || iBinarySerializable is BoraProtocol.BoraTime_DomainResponce) return 3;
            if (iBinarySerializable is BoraProtocol.BoraSoundRequest || iBinarySerializable is BoraProtocol.BoraSoundResponce) return 4;
            if (iBinarySerializable is ControlRequest || iBinarySerializable is BoraProtocol.ControlResponce) return 5;
            if (iBinarySerializable is BoraProtocol.InfoMessageRequest || iBinarySerializable is BoraProtocol.InfoMessageResponce) return 6;

            return 0;
        }


        //Шифр 1
        public async Task<BoraProtocol.BoraSpectrumResponce> GetSpectrum(double central_freq_hz = 33 * 1e6, double bandwidth_hz = 1 * 1e6, 
            double filter = 1 * 1e3, int win_cnt = 10, long time = 0, long stepTime = 100, CancellationToken token = default)
        {
            if (client != null && client.Connected)
            {
                BoraProtocol.BoraSpectrumRequest boraSpectrumRequest = new BoraProtocol.BoraSpectrumRequest()
                {
                    msg_id = _msg_id++,
                    task = new BoraProtocol.TaskRequest()
                    {
                        action = "start",
                        id = _spectrumTaskId++,
                        processors = new List<string>() { "spectrum" },
                        type = "single"
                    },
                    spectrum = new BoraProtocol.SpectrumRequest()
                    {
                        central_freq = central_freq_hz,
                        bandwidth = bandwidth_hz,
                        filter = filter,
                        win_cnt = win_cnt,
                        step_time_ms = stepTime,
                        //start_time_ms = (long)DateTime.Now.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds,
                        start_time_ms = time
                    }
                };

                byte[] message = Serialize<BoraProtocol.BoraSpectrumRequest>(boraSpectrumRequest);

                var data = await this.SendRequest(this.client, GetCode(RequestsCodes.SpectrumRequest), message, token)
                               .ConfigureAwait(false);
                var answer = data as BoraProtocol.BoraSpectrumResponce;
                return answer;
            }
            else
            {
                await this.ConnectToServerWCancelToken(this.IP, this.port, token).ConfigureAwait(false);
                return null;
            }
        }



        public delegate Task SpectrumDelegate(BoraSpectrumResponce responce);
        public async Task GetSpectrumAuto(
            SpectrumDelegate spectrumAnswerHandler,
            double central_freq_hz = 33 * 1e6,
            double bandwidth_hz = 1 * 1e6,
            double filter = 1 * 1e3,
            int win_cnt = 10,
            long time = 0,
            long stepTime = 100,
            CancellationToken token = default)
        {
            var clientSpectrum = new TcpClient();
            try
            {
#if NET5_0
                await clientSpectrum.ConnectAsync(IPAddress.Parse(IP), port, token).ConfigureAwait(false);
#else
                await clientSpectrum.ConnectAsync(IPAddress.Parse(IP), port).ConfigureAwait(false);
#endif
            }
            catch (Exception e)
            {
                this.logger.Error($"Error while connecting to server {e.Message}");
            }

            if (clientSpectrum != null && clientSpectrum.Connected)

            {
                try
                {
                    BoraProtocol.BoraSpectrumRequest boraSpectrumRequest = new BoraProtocol.BoraSpectrumRequest()
                                                                               {
                                                                                   msg_id = _msg_id++,
                                                                                   task = new BoraProtocol.TaskRequest()
                                                                                       {
                                                                                           action = "start",
                                                                                           id = _spectrumTaskId++,
                                                                                           processors =
                                                                                               new List<string>()
                                                                                                   {
                                                                                                       "spectrum"
                                                                                                   },
                                                                                       type = "single",
                                                                                       flow_mode = "asap"
                                                                                   },
                                                                                   spectrum =
                                                                                       new
                                                                                       BoraProtocol.SpectrumRequest()
                                                                                           {
                                                                                               central_freq =
                                                                                                   central_freq_hz,
                                                                                               bandwidth = bandwidth_hz,
                                                                                               filter = filter,
                                                                                               win_cnt = win_cnt,
                                                                                               step_time_ms = stepTime,
                                                                                               //start_time_ms = (long)DateTime.Now.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds,
                                                                                               start_time_ms = time
                                                                                           }
                                                                               };

                    while (!token.IsCancellationRequested)
                    {
                        try
                        {
                            byte[] message = Serialize(boraSpectrumRequest);
                            var answer =
                                (await this.SendRequest(clientSpectrum, GetCode(boraSpectrumRequest), message, token)
                                     .ConfigureAwait(false)) as BoraProtocol.BoraSpectrumResponce;
                            spectrumAnswerHandler?.Invoke(answer);
                        }
                        catch (OperationCanceledException e)
                        {
                            // ignored
                        }
                        catch (Exception ex)
                        {
                            this.logger.Error(string.Format("Error in spectrum cycle{0}", ex.Message), ex);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }

        public async Task<BoraProtocol.BoraSpectrumResponce> GetSpectrum(
            double central_freq_hz,
            double bandwidth_hz,
            long stepTime,
            long startTime = 0,
            BoraProtocol.FilterSpectrum filter = BoraProtocol.FilterSpectrum.filter_1000,
            BoraProtocol.WinCntSpectrum win_cnt = BoraProtocol.WinCntSpectrum.win_cnt_10,
            CancellationToken token = default)
        {
            return await GetSpectrum(central_freq_hz, bandwidth_hz, (double)filter, (int)win_cnt,startTime, stepTime, token);
        }

        //Шифр 2
        public async Task<BoraProtocol.BoraWaterFallResponce> GetWaterFall(
            double central_freq_hz = 33 * 1e6,
            double bandwidth_hz = 1 * 1e6,
            double filter = 1 * 1e3,
            int win_cnt = 10, CancellationToken token = default)
        {
            if (client != null && client.Connected)
            {
                var boraWaterFallRequest = new BoraProtocol.BoraWaterFallRequest()
                                               {
                                                   msg_id = _msg_id++,
                                                   task =
                                                       new BoraProtocol.TaskRequest()
                                                           {
                                                               action = "start",
                                                               id = _waterFallTaskId++,
                                                               processors = new List<string>() { "waterfall" },
                                                               type = "single"
                                                           },
                                                   waterfall = new BoraProtocol.WaterFallRequest()
                                                                   {
                                                                       central_freq = central_freq_hz,
                                                                       bandwidth = bandwidth_hz,
                                                                       filter = filter,
                                                                       win_cnt = win_cnt,
                                                                       step_time_ms = 0,
                                                                       start_time_ms =
                                                                           (long)DateTime.Now.ToUniversalTime()
                                                                               .Subtract(
                                                                                   new DateTime(
                                                                                       1970,
                                                                                       1,
                                                                                       1,
                                                                                       0,
                                                                                       0,
                                                                                       0,
                                                                                       DateTimeKind.Utc))
                                                                               .TotalMilliseconds,
                                                                       end_time_ms =
                                                                           (long)DateTime.Now.ToUniversalTime()
                                                                               .Subtract(
                                                                                   new DateTime(
                                                                                       1970,
                                                                                       1,
                                                                                       1,
                                                                                       0,
                                                                                       0,
                                                                                       0,
                                                                                       DateTimeKind.Utc))
                                                                               .TotalMilliseconds + 10,
                                                                   }
                                               };

                byte[] message = Serialize(boraWaterFallRequest);

                var data = await this.SendRequest(this.client, GetCode(RequestsCodes.WaterFallRequest), message, token)
                                               .ConfigureAwait(false);
                var answer = data as BoraProtocol.BoraWaterFallResponce;
                return answer;
            }
            else
            {
                await this.ConnectToServerWCancelToken(this.IP, this.port, token).ConfigureAwait(false);
                return null;
            }
        }

        public async Task<BoraProtocol.BoraWaterFallResponce> GetWaterFall(
            double central_freq_hz,
            double bandwidth_hz,
            BoraProtocol.FilterWaterfall filter = BoraProtocol.FilterWaterfall.filter_1000,
            BoraProtocol.WinCntWaterfall win_cnt = BoraProtocol.WinCntWaterfall.win_cnt_10,
            CancellationToken token = default)
        {
            return await this.GetWaterFall(central_freq_hz, bandwidth_hz, (double)filter, (int)win_cnt, token);
        }

        //Шифр 3
        public async Task<BoraProtocol.BoraTime_DomainResponce> GetTimeDomain(
            double central_freq_hz = 33 * 1e6,
            double bandwidth_hz = 1 * 1e6,
            double sample_rate = 1 * 1e3,
            long duration_ms = 10,
            CancellationToken token = default)
        {
            if (client != null && client.Connected)
            {
                var boraTimeDomainRequest = new BoraProtocol.BoraTime_DomainRequest()
                                                {
                                                    msg_id = _msg_id++,
                                                    task =
                                                        new BoraProtocol.TaskRequest()
                                                            {
                                                                action = "start",
                                                                id = _timeDomainTaskId++,
                                                                processors = new List<string>() { "time_domain" },
                                                                type = "single"
                                                            },
                                                    time_domain = new BoraProtocol.Time_DomainRequest()
                                                                      {
                                                                          central_freq = central_freq_hz,
                                                                          bandwidth = bandwidth_hz,
                                                                          sample_rate = sample_rate,
                                                                          start_time_ms =
                                                                              (long)DateTime.Now.ToUniversalTime()
                                                                                  .Subtract(
                                                                                      new DateTime(
                                                                                          1970,
                                                                                          1,
                                                                                          1,
                                                                                          0,
                                                                                          0,
                                                                                          0,
                                                                                          DateTimeKind.Utc))
                                                                                  .TotalMilliseconds,
                                                                          duration_ms = duration_ms,
                                                                      }
                                                };

                byte[] message = Serialize(boraTimeDomainRequest);

                var data =
                    await this.SendRequest(this.client, GetCode(RequestsCodes.TimeDomainRequest), message, token)
                        .ConfigureAwait(false);
                var answer = data as BoraProtocol.BoraTime_DomainResponce;
                return answer;
            }
            else
            {
                await this.ConnectToServerWCancelToken(this.IP, this.port, token).ConfigureAwait(false);
                return null;
            }
        }

        public async Task<BoraProtocol.BoraTime_DomainResponce> GetTimeDomain(
            double central_freq_hz,
            BoraProtocol.BandwidthTimeDomain bandwidth_hz = BoraProtocol.BandwidthTimeDomain.bandwidth_3400,
            BoraProtocol.SampleRateTimeDomain sample_rate = BoraProtocol.SampleRateTimeDomain.sample_rate_8000,
            long duration_ms = 10,
            CancellationToken token = default)
        {
            return await GetTimeDomain(central_freq_hz, (double)bandwidth_hz, (double)sample_rate, duration_ms, token);
        }

        //Шифр 4
        public async Task<BoraProtocol.BoraSoundResponce> GetSound(
            double central_freq_hz = 33 * 1e6,
            string demod = "Am",
            double bandwidth_hz = 1 * 1e6,
            double sample_rate = 1 * 1e3,
            BoraProtocol.GainControl gainControl = default,
            double agc_level = 1.0,
            double mgc_coef = 1.0,
            long duration_ms = 10,
            CancellationToken token = default)
        {
            if (client != null && client.Connected)
            {
                BoraProtocol.BoraSoundRequest boraSoundRequest = new BoraProtocol.BoraSoundRequest()
                {
                    msg_id = _msg_id++,
                    task = new BoraProtocol.TaskRequest()
                    {
                        action = "start",
                        id = _soundTaskId++,
                        processors = new List<string>() { "sound" },
                        type = "single"
                    },
                    sound = new BoraProtocol.Sound()
                    {
                        central_freq = central_freq_hz,
                        demod_name = demod.ToString(),
                        bandwidth = bandwidth_hz,
                        sample_rate = sample_rate,
                        gain_control = gainControl.ToString(),
                        agc_level = agc_level,
                        mgc_coef = mgc_coef,
                        start_time_ms = -1020,
                        duration_ms = 1000,
                    }
                };

                byte[] message = Serialize(boraSoundRequest);

                var data =
                    await this.SendRequest(this.client, GetCode(boraSoundRequest), message, token).ConfigureAwait(false);
                var answer = data as BoraProtocol.BoraSoundResponce;
                return answer;
            }
            else
            {
                await this.ConnectToServerWCancelToken(this.IP, this.port, token).ConfigureAwait(false);
                return null;
            }
        }

        public delegate Task SoundDelegate(BoraSoundResponce responce);
        public async Task GetSoundAuto(
            SoundDelegate soundAnswerHandle,
            double central_freq_hz = 33 * 1e6,
            string demod = "Am",
            double bandwidth_hz = 1 * 1e6,
            double sample_rate = 1 * 1e3,
            BoraProtocol.GainControl gainControl = default,
            double agc_level = 1.0,
            double mgc_coef = 1.0,
            long startTime = 0,
            long duration_ms = 10,
            CancellationToken token = default)
        {
            var clientSound = new TcpClient();
            try
            {
#if NET5_0
                await clientSound.ConnectAsync(IPAddress.Parse(IP), port, token).ConfigureAwait(false);
#else
                await clientSound.ConnectAsync(IPAddress.Parse(IP), port).ConfigureAwait(false);
#endif
            }
            catch (Exception e)
            {
                this.logger.Error($"Error while connecting to server {e.Message}");
            }

            if (clientSound != null && clientSound.Connected)

            {
                var msg_id = _msg_id++;
                try
                {
                    BoraProtocol.BoraSoundRequest boraSoundRequest = new BoraProtocol.BoraSoundRequest()
                                                                         {
                                                                             msg_id = msg_id,
                                                                             task = new BoraProtocol.TaskRequest()
                                                                                 {
                                                                                     action = "start",
                                                                                     id = _soundTaskId++,
                                                                                     processors =
                                                                                         new List<string>()
                                                                                             {
                                                                                                 "sound"
                                                                                             },
                                                                                     type = "stream",
                                                                                     flow_mode = "rt"
                                                                                 },
                                                                             sound = new BoraProtocol.Sound()
                                                                                 {
                                                                                     central_freq = central_freq_hz,
                                                                                     demod_name = demod.ToString(),
                                                                                     bandwidth = bandwidth_hz,
                                                                                     sample_rate = sample_rate,
                                                                                     gain_control =
                                                                                         gainControl.ToString(),
                                                                                     agc_level = agc_level,
                                                                                     mgc_coef = mgc_coef,
                                                                                     start_time_ms = startTime,
                                                                                     duration_ms = duration_ms,
                                                                                 }
                                                                         };

                    byte[] message = Serialize(boraSoundRequest);
                    await this.SendRequestWithoutAnswer(clientSound,GetCode(boraSoundRequest), message, token).ConfigureAwait(false);
                    while (!token.IsCancellationRequested)
                    {
                        try
                        {
                            var data = await this.ReadMessage(clientSound, token).ConfigureAwait(false);
                            var answer = data as BoraProtocol.BoraSoundResponce;
                            soundAnswerHandle?.Invoke(answer);
                        }
                        catch (OperationCanceledException e)
                        {
                            // ignored
                        }
                        catch (Exception ex)
                        {
                            this.logger.Error(string.Format("Error in sound cycle{0}", ex.Message), ex);
                        }
                    }

                    boraSoundRequest.task.action = "stop";
                    boraSoundRequest.sound = null;
                    message = Serialize(boraSoundRequest);
                    await this.SendRequestWithoutAnswer(clientSound,GetCode(boraSoundRequest), message, default)
                        .ConfigureAwait(false);

                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }



        public async Task<BoraProtocol.BoraSoundResponce> GetSound(
            double central_freq_hz,
            BoraProtocol.Demod demod,
            BoraProtocol.BandwidthSound bandwidth_hz = BoraProtocol.BandwidthSound.bandwidth_3400,
            BoraProtocol.SampleRateSound sample_rate = BoraProtocol.SampleRateSound.sample_rate_8000,
            BoraProtocol.GainControl gainControl = default,
            double agc_level = 1.0,
            double mgc_coef = 1.0,
            long duration_ms = 10,
            CancellationToken token = default)
        {
            return await this.GetSound(
                       central_freq_hz,
                       demod.ToString(),
                       (double)bandwidth_hz,
                       (double)sample_rate,
                       gainControl,
                       agc_level,
                       mgc_coef,
                       duration_ms,
                       token).ConfigureAwait(false);
        }

        //Шифр 5
        private BoraProtocol.TaskRequest ControlTaskRequest(BoraProtocol.ControlDevice controlDevice)
        {
            return new BoraProtocol.TaskRequest()
            {
                action = "start",
                id = _controlTaskId++,
                processors = new List<string>() { BoraProtocol.InfoProcessors.control.ToString() },
                type = "single"
            };
        }
        private BoraProtocol.Control ControlRequest(BoraProtocol.ControlDevice controlDevice, int attValue, BoraProtocol.OnOff onOff)
        {
            return new BoraProtocol.Control()
            {
                devices = new List<string>() { controlDevice.ToString() },
                att = new BoraProtocol.Att()
                {
                    value = AttValue()
                }
            };

            string AttValue()
            {
                switch (controlDevice)
                {
                    case BoraProtocol.ControlDevice.att:
                        return attValue.ToString();
                    case BoraProtocol.ControlDevice.amplifier:
                    case BoraProtocol.ControlDevice.commutator_switch:
                        return onOff.ToString();
                    default:
                        return "";
                }
            }
        }

        public async Task<BoraProtocol.ControlResponce> SetAttControl(int attValue, CancellationToken token = default)
        {
            if (client != null && client.Connected)
            {
                BoraProtocol.ControlRequest controlRequest = new BoraProtocol.ControlRequest()
                {
                    msg_id = _msg_id++,
                    task = ControlTaskRequest(BoraProtocol.ControlDevice.att),
                    control = ControlRequest(BoraProtocol.ControlDevice.att, attValue, default)
                };

                byte[] message = Serialize(controlRequest);

                var data =
                    await this.SendRequest(this.client, GetCode(controlRequest), message, token).ConfigureAwait(false);
                var answer = data as BoraProtocol.ControlResponce;
                return answer;
            }
            else
            {
                await this.ConnectToServerWCancelToken(this.IP, this.port, token).ConfigureAwait(false);
                return null;
            }
        }
        public async Task<BoraProtocol.ControlResponce> SetAmplifierControl(BoraProtocol.OnOff onOff, CancellationToken token = default)
        {
            if (client != null && client.Connected)
            {
                BoraProtocol.ControlRequest controlRequest = new BoraProtocol.ControlRequest()
                {
                    msg_id = _msg_id++,
                    task = ControlTaskRequest(BoraProtocol.ControlDevice.amplifier),
                    control = ControlRequest(BoraProtocol.ControlDevice.amplifier, default, onOff)
                };

                byte[] message = Serialize(controlRequest);

                var data =
                    await this.SendRequest(this.client, GetCode(controlRequest), message, token).ConfigureAwait(false);
                var answer = data as BoraProtocol.ControlResponce;
                return answer;
            }
            else
            {
                await this.ConnectToServerWCancelToken(this.IP, this.port, token).ConfigureAwait(false);
                return null;
            }
        }
        public async Task<BoraProtocol.ControlResponce> SetCommutatorSwitchControl(BoraProtocol.OnOff onOff, CancellationToken token = default)
        {
            if (client != null && client.Connected)
            {
                BoraProtocol.ControlRequest controlRequest = new BoraProtocol.ControlRequest()
                {
                    msg_id = _msg_id++,
                    task = ControlTaskRequest(BoraProtocol.ControlDevice.commutator_switch),
                    control = ControlRequest(BoraProtocol.ControlDevice.commutator_switch, default, onOff)
                };

                byte[] message = Serialize(controlRequest);

                var data = await this.SendRequest(this.client, GetCode(controlRequest), message, token).ConfigureAwait(false);
                var answer = data as BoraProtocol.ControlResponce;
                return answer;
            }
            else
            {
                await this.ConnectToServerWCancelToken(this.IP, this.port, token).ConfigureAwait(false);
                return null;
            }
        }

        public async Task<BoraProtocol.ControlResponce> SetControlTest(CancellationToken token = default)
        {
            if (client != null && client.Connected)
            {
                BoraProtocol.ControlRequest controlRequest = new BoraProtocol.ControlRequest()
                {
                    msg_id = _msg_id++,
                    task = new BoraProtocol.TaskRequest()
                    {
                        action = "start",
                        id = _controlTaskId++,
                        processors = new List<string>() { "control" },
                        type = "single"
                    },
                    control = new BoraProtocol.Control()
                    {
                        devices = new List<string>() { "att" },
                        att = new BoraProtocol.Att()
                        {
                            value = "10"
                        }
                    }
                };

                byte[] message = Serialize(controlRequest);

                var data = await this.SendRequest(this.client, GetCode(controlRequest), message, token).ConfigureAwait(false);
                var answer = data as BoraProtocol.ControlResponce;
                return answer;
            }
            else
            {
                await this.ConnectToServerWCancelToken(this.IP, this.port, token).ConfigureAwait(false);
                return null;
            }
        }

        //Шифр 6
        public async Task<BoraProtocol.InfoMessageResponce> GetInfo(CancellationToken token = default)
        {
            List<BoraProtocol.InfoProcessors> infoProcessors = Enums<BoraProtocol.InfoProcessors>();
            return await GetInfo(infoProcessors, token);
        }

        public async Task<BoraProtocol.InfoMessageResponce> GetInfo(
            BoraProtocol.InfoProcessors infoProcessor,
            CancellationToken token = default)
        {
            return await GetInfo(new[] { infoProcessor }, token);
        }

        public async Task<BoraProtocol.InfoMessageResponce> GetInfo(
            IEnumerable<BoraProtocol.InfoProcessors> infoProcessors,
            CancellationToken token = default)
        {
            if (client != null && client.Connected)
            {
                var infoMessageRequest = new BoraProtocol.InfoMessageRequest()
                                             {
                                                 msg_id = _msg_id++,
                                                 task = new BoraProtocol.TaskRequest()
                                                            {
                                                                action = "start",
                                                                id = _infoTaskId++,
                                                                processors = new List<string>() { "info" },
                                                                type = "single"
                                                            },
                                                 info = new BoraProtocol.InfoProcessorsRequest()
                                                            {
                                                                processors = infoProcessors.Distinct()
                                                                    .Select(x => x.ToString()).ToList(),
                                                            },
                                             };

                byte[] message = Serialize(infoMessageRequest);

                var data =
                    await this.SendRequest(this.client, GetCode(infoMessageRequest), message, token).ConfigureAwait(false);
                var answer = data as BoraProtocol.InfoMessageResponce;
                return answer;
            }
            else
            {
                await this.ConnectToServerWCancelToken(this.IP, this.port, token).ConfigureAwait(false);
                return null;
            }
        }
    }
}
