﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BoraClientDll
{
    public partial class BoraProtocol
    {
        public static Dictionary<int, string> EnumNamedValues<T>() where T : System.Enum
        {
            var result = new Dictionary<int, string>();
            var values = Enum.GetValues(typeof(T));

            foreach (int item in values)
                result.Add(item, Enum.GetName(typeof(T), item));
            return result;
        }

        public static List<T> Enums<T>() where T : Enum
        {
            var values = Enum.GetValues(typeof(T));
            return values.Cast<T>().ToList();
        }

        public enum ConnectionStatus
        {
            Connected,
            Error,
            ServerNotResponding,
            InvalidIp
        }

        public enum TaskType
        {
            single,
            stream
        }

        public enum TaskAction
        {
            start,
            change,
            stop
        }

        public enum TaskState
        {
            active,
            cancelled,
            finished
        }

        public enum TaskProcessors
        {
            spectrum,
            waterfall,
            time_domain,
            sound,
            control,
            info
        }

        public enum InfoProcessors
        {
            spectrum,
            waterfall,
            time_domain,
            sound,
            control,
        }

        public enum TaskFlowMode
        {
            asap,
            rt
        }

        public enum ControlDevice
        {
            att,
            amplifier,
            commutator_switch
        }
        public enum AttValues
        {
            Zero = 0,
            Ten = 10,
            Twenty = 20,
            Thirty = 30,
        }
        public enum OnOff
        {
            off,
            on,
        }

        public enum Demod
        {
            AM,
            FM,
            LSB,
            USB
        }

        public enum GainControl
        {
            auto,
            manual
        }

        public enum BandwidthSound
        {
            bandwidth_3400 = 3400,
            bandwidth_10000 = 10000,
        }

        public enum SampleRateSound
        {
            sample_rate_8000 = 8000,
            sample_rate_9600 = 9600,
            sample_rate_24000 = 24000,
        }

        public enum FilterWaterfall
        {
            filter_25 = 25,
            filter_100 = 100,
            filter_500 = 500,
            filter_1000 = 1000,
            filter_5000 = 5000
        }
        public enum WinCntWaterfall
        {
            win_cnt_1 = 1,
            win_cnt_2 = 2,
            win_cnt_4 = 4,
            win_cnt_10 = 10
        }

        public enum BandwidthTimeDomain
        {
            bandwidth_3400 = 3400,
            bandwidth_21000 = 21000,
        }

        public enum SampleRateTimeDomain
        {
            sample_rate_8000 = 8000,
            sample_rate_24000 = 24000,
        }

        public enum FilterSpectrum
        {
            filter_100 = 100,
            filter_500 = 500,
            filter_1000 = 1000,
        }
        public enum WinCntSpectrum
        {
            win_cnt_1 = 1,
            win_cnt_2 = 2,
            win_cnt_4 = 4,
            win_cnt_10 = 10
        }
    }
}
